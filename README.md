Youtube Subtitle Viewer

URL: https://hiogawa.gitlab.io/ytsub

```
# Development
$ npm install
$ bash scripts/patch.sh
$ npm start

# Local cors proxy
$ npm run proxy

# Deployment (cf. https://docs.gitlab.com/ee/user/project/pages/)
```

References

- https://developers.google.com/youtube/iframe_api_reference
- https://github.com/youtube/api-samples/tree/master/player
- https://github.com/rg3/youtube-dl/blob/master/youtube_dl/extractor/youtube.py#L1237
- https://developers.google.com/web/progressive-web-apps/checklist

Examples URLs

- https://www.youtube.com/watch?v=4fpOXK3Znxs
- https://video.google.com/timedtext?hl=en&type=list&v=IteFQdZgGFQ
- https://www.youtube.com/api/timedtext?lang=ru&v=IteFQdZgGFQ&fmt=ttml

Example ttml files under `misc/`

How to generate favicon icon

```
$ wget https://raw.githubusercontent.com/FortAwesome/Font-Awesome/master/svgs/solid/closed-captioning.svg
$ convert -size 32x32 -background none closed-captioning.svg favicon.png
```

How to make patch

```
$ cp node_modules/backbone/backbone.js src/shims/backbone.js
$ # edit src/shims/backbone.js ...
$ diff -u  node_modules/backbone/backbone.js src/shims/backbone.js > scripts/backbone.patch
```

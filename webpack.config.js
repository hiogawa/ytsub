const path = require('path');
const webpack = require('webpack');
const _ = require('underscore');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const PRODUCTION = process.env.BUILD_ENV == 'production';
const outputPath = path.resolve(__dirname, PRODUCTION ? 'public' : 'out');
const publicPath = PRODUCTION ? '/ytsub/' : '/';

module.exports = {
    mode: (PRODUCTION ? 'production' : 'development'),
    entry: { index: './src/index.js' },
    output: {
        filename: (PRODUCTION ? '[name].[hash].js' : '[name].js'),
        path: outputPath,
        publicPath,
    },
    resolve: {
        alias: {
          'jquery': path.resolve(__dirname, 'src/shims/jquery.js'), // fake for backbone
          'backbone': path.resolve(__dirname, 'src/shims/backbone.js') // patched backbone
        }
    },
    plugins: [
        new webpack.DefinePlugin(_.mapObject({
            PRODUCTION: PRODUCTION,
            PUBLIC_PATH: publicPath,
            PROXY_PREFIX: 'https://script.google.com/macros/s/AKfycbwlRhtH1THiHcTY0hbZtcMd1K_ucndHfa-8iugHJMgaKjDY2HqoJbMAACMIITNeMNpZ/exec?url=',
        }, JSON.stringify)),
        new HtmlWebpackPlugin({ template: 'src/index.html' }),
        new webpack.HotModuleReplacementPlugin(),
        new CopyPlugin([
            { from: 'src/assets/*.png', to: outputPath, flatten: true },
            { from: 'src/assets/manifest.json', to: outputPath, flatten: true,
              transform: (content) => _.template(content.toString())({ publicPath }) },
        ]),
    ],
    module: {
        rules: [
            {
                enforce: "pre",
                test: /\.js$/,
                include: [ path.join(__dirname, 'src') ],
                exclude: [ path.join(__dirname, 'src/shims') ],
                loader: 'eslint-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [ path.join(__dirname, 'src') ],
                exclude: [ path.join(__dirname, 'src/shims') ],
                options: {
                    cacheDirectory: true,
                    presets: ['@babel/preset-react'],
                    plugins: ['react-hot-loader/babel']
                },
            },
            {
                test: /\.scss$/,
                use: [ 'style-loader', 'css-loader', 'sass-loader' ]
            }
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    devtool: (PRODUCTION ? 'source-map' : 'inline-source-map'),
    devServer: {
        contentBase: './out',
        historyApiFallback: true,
        hot: true,
        port: 8080
    },
};

// Usage:
// $ node scripts/scrape.js UkcvsV8V_iA pd6XrIs7Xuw 9s8b-0nFF4I mWPMDuIG47I > src/assets/recommendedVideos.json

const child_process = require('child_process');

const video_ids = process.argv.slice(2);

const arr = video_ids.map(id => {
  const buf = child_process.execSync(`curl -H 'Accept-Language: en-US,en' https://www.youtube.com/watch?v=${id}`);
  return {
    id: id,
    config: JSON.parse(buf.toString().match(/;ytplayer\.config\s*=\s*({.+?});ytplayer/)[1])
  };
});

console.log(JSON.stringify(arr, null, 2));

function getSheetByVideoId(videoId) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var row = _.find(_.zip(
    _.flatten(ss.getRangeByName('DeckStatistics_DeckName').getValues()),
    _.flatten(ss.getRangeByName('DeckStatistics_VideoId').getValues())
  ), function(row) { return row[1] == videoId; });
  return row ? ss.getSheetByName(row[0]) : null;
}

function doGet(e) {
  return ContentService.createTextOutput(JSON.stringify({ text: 'Hello, world!'})).setMimeType(ContentService.MimeType.JSON);
  if (!(e.parameter.videoId && e.parameter.sentenceIndex)) {
    return HtmlService.createHtmlOutput('<b>Please specify "videoId" and "sentenceIndex".</b>');
  }

  var videoId = e.parameter.videoId;
  var sentenceIndex = e.parameter.sentenceIndex;
  var sheet = getSheetByVideoId(videoId);
  if (!sheet) {
    return HtmlService.createHtmlOutput('<b>"videoId" is invalid.</b>');
  }

  var entries = sheet.getRange(1, 1, sheet.getLastRow(), 6).getValues();
  var row = _.find(entries, function(row) { return row[3].toString() === sentenceIndex; });
  if (!row) {
    return HtmlService.createHtmlOutput('<b>"sentenceIndex" is invalid.</b>');
  }

  var tmpl = HtmlService.createTemplateFromFile('index');
  tmpl.D = {
    videoId: videoId,
    sentenceIndex: sentenceIndex,
    start: TimeStampToSecond(row[4]),
    entries: entries
  };
  return tmpl.evaluate();
}

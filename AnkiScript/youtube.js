function ExtractYTPlayerConfig(content) {
  var mobj = content.match(/;ytplayer\.config\s*=\s*({.+?});ytplayer/);
  return JSON.parse(mobj[1]);
}

// TODO
function DownloadMetadata(videoId) {
  var response = UrlFetchApp.fetch(
    'https://www.youtube.com/watch?v=' + videoId,
    { headers: { 'Accept-Language': 'en-US,en' } }
  );
  var config = ExtractYTPlayerConfig(response.getContentText());
  // thumbnail, title, author ...
}

function DownloadSubtitleList(videoId) {
  var response = UrlFetchApp.fetch(
      'https://www.youtube.com/watch?v=' + videoId,
      { headers: { 'Accept-Language': 'en-US,en' } }
  );
  var config = ExtractYTPlayerConfig(response.getContentText());
  var player_response = JSON.parse(config.args.player_response);
  var tracks = player_response.captions.playerCaptionsTracklistRenderer.captionTracks;
  var translations = player_response.captions.playerCaptionsTracklistRenderer.translationLanguages;
  var list = [];
  tracks.forEach(function(track) {
    list.push([
      track.name.simpleText,
      track.baseUrl + "&fmt=ttml"
    ]);
    if (track.isTranslatable) {
      translations.forEach(function(translation) {
        list.push([
          track.name.simpleText + '\n=> ' + translation.languageName.simpleText,
          track.baseUrl + "&fmt=ttml&tlang=" + translation.languageCode
        ]);
      });
    }
  });
  return list;
}

function DownloadSubtitle(url) {
  var xml = UrlFetchApp.fetch(url).getContentText();
  var document = XmlService.parse(xml);
  var ns = XmlService.getNamespace('http://www.w3.org/ns/ttml');
  var root = document.getRootElement();
  var ps = root.getChild('body', ns).getChild('div', ns).getChildren('p', ns);
  return ps.map(function(p) {
    var begin = p.getAttribute('begin').getValue();
    var end = p.getAttribute('end').getValue();
    // Deal with <br />
    var text = p.getAllContent().map(function(c) { return c.getText(); }).filter(Boolean).join(' ')
    return [text, begin, end];
  });
}

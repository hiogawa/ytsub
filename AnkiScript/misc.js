var _ = LodashGS.load();

function Debug(s) {
  SpreadsheetApp.getUi().alert(s);
}

function TimeStampToSecond(date) {
  return date.getMinutes() * 60 + date.getSeconds();
}

function LinkToRange(a1Notation) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var range = ss.getRange(a1Notation);
  return ss.getUrl() + '#gid=' + range.getGridId() + '&range=' + range.getA1Notation();
}

// e.g. =DeckStatistics('1'!C:C)
function DeckStatistics(rateLogs) {
  var rateLogs = _.filter(_.flatten(rateLogs));
  return [[
    _.filter(rateLogs, function(rate) { return rate.match(/^Z/); }),
    _.filter(rateLogs, function(rate) { return rate.match(/^A/); }),
    _.filter(rateLogs, function(rate) { return rate.match(/^B/); }),
    _.filter(rateLogs, function(rate) { return rate.match(/^C/); }),
    _.filter(rateLogs, function(rate) { return rate.match(/^W/); }),
    _.filter(rateLogs, function(rate) { return rate }),
  ].map(function(ar) { return ar.length })];
}

function InitializeDeck() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();
  _InitializeDeck(sheet);
}

function _InitializeDeck(sheet) {
  var last = sheet.getLastRow();

  // Move timestamp field to the right
  sheet.getRange(1, 3, last, 2).moveTo(sheet.getRange(1, 5, last, 2));

  // Setup initial rate-log and index
  var values = _.range(last).map(function(i) { return ['Z', i + 1]; });
  sheet.getRange(1, 3, last, 2)
    .setValues(values);

  // Style up cells (sentence cell width 200, alignment top-left, font-size 12)
  sheet.getRange(1, 1, last, 6)
    .setVerticalAlignment('top')
    .setHorizontalAlignment('left')
    .setFontSize(12);
  sheet.getRange(1, 1, last, 2)
    .setWrap(true)
  sheet.setColumnWidths(1, 2, 200);
}

function SetNextSentenceAtRandom() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var deckName = GetNextDeck();
  var sheet = ss.getSheetByName(deckName);
  SetDeckAndRowIndex(
    deckName,
    Math.ceil(Math.random() * sheet.getLastRow())
  );
}

function SetNextSentenceSmartly() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var deckName = GetNextDeck();
  SetDeckAndRowIndex(
    deckName,
    GetNextRowIndexSmartly(deckName)
  );
}

function GetNextDeck() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  if (ss.getRangeByName('DeckMixMode').isChecked()) {
    var selectedIndeces =
        ss.getRangeByName('DeckStatistics_MixMode').getValues()
        .map(function(row, idx) { return [row[0], idx]; })
        .filter(function(val_idx) { return val_idx[0]; })
        .map(function(val_idx) { return val_idx[1]; });
    if (selectedIndeces.length === 0) {
      Debug('No deck is selected for mixing.');
      return ss.getRangeByName('Deck').getValue();
    }
    return ss.getRangeByName('DeckStatistics_DeckName').getValues()[_.sample(selectedIndeces)][0]
  } else {
    return ss.getRangeByName('Deck').getValue();
  }
}

// Update "Deck" and "RowIndex" within the same transaction
function SetDeckAndRowIndex(deck, rowIndex) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  ss.getRangeByName('DeckAndRowIndex').setValues([[deck], [rowIndex]]);
}

function GetNextRowIndexSmartly(deckName) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName(deckName);
  var originalSentences = sheet.getRange(1, 1, sheet.getLastRow(), 4).getValues();
  var sentences;

  // Ignore "W" rated sentences
  // Order sentences by;
  // - not-yet-practiced sentence frist
  // - then descending order by its rate of last paractice
  // Also each group of sentences with same score is shuffled
  sentences = _.filter(originalSentences, function(s) { return s[2].match('W'); })
  sentences = _.shuffle(originalSentences);
  sentences = _.sortBy(sentences, function(s) { return s[2]; });
  sentences = _.reverse(sentences);
  return 1 + _.findIndex(originalSentences, function(s) { return s[3] === sentences[0][3] });
}

function Rate(v) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheetName = ss.getRangeByName('Deck').getValue();
  var sheet = ss.getSheetByName(sheetName);
  var rowIndex = ss.getRangeByName('RowIndex').getValue();
  var logCell = sheet.getRange(rowIndex, 3);
  logCell.setValue(v + logCell.getValue());

  var practiceLog = ss.getSheetByName('PracticeLog');
  practiceLog.getRange(practiceLog.getLastRow() + 1, 1, 1, 4).setValues([[
    new Date(),
    v,
    sheetName,
    sheet.getRange(rowIndex, 4).getValue()
  ]]);
}

// Only for migrating the initial data
function InitializePracticeLog() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('PracticeLog');
  var date = new Date('2019-07-29T12:00:00.000+09:00');
  [ '1', '2', '3', '4', '5' ].forEach(function(deckName) {
    var deck = ss.getSheetByName(deckName);
    deck.getRange(1, 1, deck.getLastRow(), 4).getValues().forEach(function(row) {
      var lastRate = row[2][0];
      if (_.includes(['A', 'B', 'C'], lastRate)) {
        sheet.getRange(sheet.getLastRow() + 1, 1, 1, 4).setValues([[
          date,
          lastRate,
          deckName,
          row[3],
        ]]);
      }
    });
  });
}

function GenerateDeck() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var deckName = ss.getRangeByName('DeckGenerator_DeckName').getValue();
  var sheet = ss.insertSheet(deckName.toString(), 6);
  var lefts = ss.getRangeByName('DeckGenerator_Left').getValues();
  var rights = ss.getRangeByName('DeckGenerator_Right').getValues();
  var values = _.zipWith(lefts, rights, function(left, right) {
    return _.concat(left[0], right);
  });
  sheet.getRange(1, 1, values.length, 4).setValues(values);
  _InitializeDeck(sheet);
}

// UI Event handlers
function onEdit(e) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();

  var range = ss.getRangeByName('NextSmartly');
  if (range.isChecked()) {
    SetNextSentenceSmartly();
    range.uncheck();
  }

  var range = ss.getRangeByName('NextAtRandom');
  if (range.isChecked()) {
    SetNextSentenceAtRandom();
    range.uncheck();
  }

  var range = ss.getRangeByName('RateA');
  if (range.isChecked()) {
    Rate('A');
    SetNextSentenceSmartly();
    range.uncheck();
  }
  var range = ss.getRangeByName('RateB');
  if (range.isChecked()) {
    Rate('B');
    SetNextSentenceSmartly();
    range.uncheck();
  }
  var range = ss.getRangeByName('RateC');
  if (range.isChecked()) {
    Rate('C');
    SetNextSentenceSmartly();
    range.uncheck();
  }
  var range = ss.getRangeByName('RateW');
  if (range.isChecked()) {
    Rate('W');
    SetNextSentenceSmartly();
    range.uncheck();
  }

  var range = ss.getRangeByName('DeckGenerator_Generate');
  if (range.isChecked()) {
    GenerateDeck();
    range.uncheck();
  }
}

Making some simpler version of [Anki](https://apps.ankiweb.net/) within Google sheet.

- https://docs.google.com/spreadsheets/d/177FpBuZg0Thb-4JduHj8A0X3FsKvNoYbEgyZ9gUzCbw/edit?usp=sharing

---

Some Ideas

- Ability to add entry with video timestamp and to replay it somehow (in iframe?)
- Ability to collect sentence as phrases or vocabulary basis, instead of decks
- Ability to skip/delete useless sentence from a deck
- Make AnkiScript as independent app
  - Standalone script, add-on, webapp?
  - Data structure consideration? (database or spreadsheet)
  - Make new website from scratch, only borrowing idea/concept from this?

---

"Web app" idea

First step:
- it's one-off page (i.e. just open it from "Practice" then allow to rate one sentence and close the page).

Parameter would be:
- video id
- sentence index

Minimal functionality:
- iframe
- subtitles
- loop/jump to sentence
- rate current sentence

Notes:
- rating can be done via "google.script.run".


---

References

- https://github.com/google/clasp
- https://github.com/oshliaer/lodashgs
- https://developers.google.com/apps-script/concepts/manifests
- https://developers.google.com/apps-script/reference/spreadsheet/spreadsheet-app
- https://developers.google.com/apps-script/reference/spreadsheet/spreadsheet
- https://developers.google.com/apps-script/reference/spreadsheet/range
- https://developers.google.com/apps-script/guides/triggers/
- https://developers.google.com/apps-script/guides/html/

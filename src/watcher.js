import _ from 'underscore';
import objectPath from 'object-path';

export const watch = (store, path, predicate, callback, options = {}) => {
    options = _.extend({ once: false }, options);
    let getValue = () => objectPath.get(store.getState(), path);
    let current = getValue();
    let unsubscribe = store.subscribe(() => {
        let previous = current;
        current = getValue();
        if (current !== previous && predicate(current)) {
            callback(current);
            if (options.once) {
                unsubscribe();
            }
        }
    });
};

export const initializeWatchers = (store, router) => {
    watch(store, 'video.status', (status) => status === 'ERROR', () => {
        window.alert('Error: Video information cannot be obtained.')
    });
    watch(store, '_persist.rehydrated', Boolean, () => {
        router.start();
    }, { once: true });
};

import React from 'react';
import _ from 'underscore';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Modal from '@material-ui/core/Modal';
import { actions } from '../stateUtils.js';
import { navigate, navigateBack } from '../Router.js';
import Icon from './Icon.js';
import Input from './Input.js';
import { parseYoutubeUrl, extractVideoInfoFromConfig } from '../utils.js';
import recommendedVideos from '../assets/recommendedVideos.json';


const Item = (props) => {
    let { id, config, onClose } = props;
    let { author, title, channelId } = extractVideoInfoFromConfig(config);
    // NOTE (2019/06/06)
    // It seems config object's thumbnail url attribute has changed recently.
    // For now, just find it directly from video id.
    // NOTE (2019/08/09)
    // data is now lives in JSON.parse(config.args.player_response).videoDetails
    let cover_url = `https://i.ytimg.com/vi/${id}/hqdefault.jpg`
    return (
        <div className='home-item' onClick={() => navigate(`watch/${id}`)}>
            { onClose &&
                <Icon icon='close' className='home-item__remove'
                    onClick={(e) => { e.stopPropagation(); onClose() }}
                />
            }
            <div className='home-item__cover'>
                <img src={cover_url} />
            </div>
            <div className='home-item__title'>{ title }</div>
            <div className='home-item__author' onClick={(e) => e.stopPropagation()}>
                <a title="Open author's channel"
                    href={`https://www.youtube.com/channel/${channelId}`}
                    target='_blank' rel="noopener noreferrer">{ author }</a>
            </div>
        </div>
    );
}

const loadVideo = (value) => {
    let videoId = parseYoutubeUrl(value);
    if (videoId) {
        navigate(`watch/${videoId}`);
    } else {
        window.alert('Error: Your input is invalid.');
    }
}

const HomeModal = ({ open, isInitialLoad, history, deleteHistory }) => (
    <Modal {...{ open }} onClose={() => !isInitialLoad && navigateBack()}>
        <div className='home'>
            { !isInitialLoad &&
                <Icon icon='close' className='home__close' onClick={navigateBack} />}
            <div className='home__header'>
                Load Video
            </div>
            <div className='home__body--with-input'>
                <Input onValueEntered={loadVideo} placeholder='ID or URL' />
            </div>
            <div className='home__header'>
                Watch History
            </div>
            <div className='home__body'>
                { history.length === 0
                  ? <div style={{ marginLeft: '40px' }}>You do not have watch history.</div>
                  : history.map(({ id, config }) =>
                      <Item key={id} {...{ id, config }} onClose={() => deleteHistory(id)} />)}
            </div>
            <div className='home__header'>
                Recommended
            </div>
            <div className='home__body'>
                { recommendedVideos.map(({ id, config }) => <Item key={id} {...{ id, config }} />)}
            </div>
        </div>
    </Modal>
);

export default connect(
    ({ home: { open, isInitialLoad }, history }) => ({ open, isInitialLoad, history }),
    D => bindActionCreators(_.pick(actions, ['deleteHistory']), D)
)(HomeModal)

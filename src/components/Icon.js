import React from 'react';

const Icon = ({icon, ...props}) => (
    <span {...props}>
        <i className="material-icons">{icon}</i>
    </span>
);

export default Icon;

import React, { Component } from 'react';
import _ from 'underscore';
import update from 'immutability-helper';
import CN from 'classnames';
import Icon from './Icon.js';

export default class TimedTextEntry extends Component {
    constructor(props) {
        super(props);

        let { lang1_text } = this.props;
        let choices = _.compact(lang1_text.split(' ')).map(w => ({ word: w, id: _.uniqueId() }));
        this.state = {
            choices: _.shuffle(choices),
            user_answer: [],
            revealed: false
        };
    }
    $setState(command) {
        this.setState(update(this.state, command));
    }
    selectWord(obj) {
        this.$setState({ user_answer: { $push: [ obj ] } });
    }
    deselectWord(obj) {
        let { user_answer } = this.state;
        let i = _.findIndex(user_answer, x => x == obj);
        this.$setState({ user_answer: { $splice: [[i, 1]] } });
    }
    resetPractice() {
        this.setState({
            choices: _.shuffle(this.state.choices),
            user_answer: [],
            revealed: false
        });
    }
    render() {
        let {
            begin,        end,
            lang1_text,   lang2_text,
            onTogglePlay, onToggleLoop, onTogglePractice, onChangePractice,
            focusing,     practicing,   looping,          playing
        } = this.props;
        let { choices, user_answer, revealed } = this.state;
        return (
            <div className={CN('timed-text-entry', { focusing, looping })}>
                <div className="timed-text-entry__header">
                    <Icon icon={'play_circle_outline'} title='Play'
                        className={CN('control', { 'enabled': playing })}
                        onClick={onTogglePlay} />
                    <Icon icon={'repeat'} title='Loop'
                        className={CN('control', { 'enabled': looping })}
                        onClick={onToggleLoop} />
                    <Icon icon={'edit'} title="Practice"
                        className={CN('control', { 'enabled': practicing })}
                        onClick={onTogglePractice} />
                    <div className="time">{ begin } - { end }</div>
                </div>
                <div className="timed-text-entry__body">
                    <div>{ practicing && !revealed
                            ? user_answer.map(_.property('word')).join(' ')
                            : lang1_text }</div>
                    <div>{ lang2_text }</div>
                </div>
                { practicing &&
                    <div className="timed-text-entry__practice-box practice-box">
                        <div className="practice-box__body">
                            { choices.map(obj => {
                                let selected = _.contains(user_answer, obj);
                                let method = selected ? 'deselectWord' : 'selectWord';
                                return (
                                    <div key={obj.id} className={CN({ selected: revealed || selected })}
                                        onClick={() => revealed || this[method](obj)}
                                    > { obj.word } </div>
                                );
                            })}
                        </div>
                        <div className="practice-box__footer">
                            <div onClick={() => onChangePractice(-1)}>
                                <Icon icon={'arrow_back'} />
                            </div>
                            <div onClick={onTogglePractice}>
                                <Icon icon={'close'} />
                            </div>
                            <div className={CN({ enabled: revealed })}
                                onClick={() => this.setState({ revealed: !revealed })}>
                                <Icon icon={'spellcheck'} />
                            </div>
                            <div onClick={() => this.resetPractice()}>
                                <Icon icon={'refresh'} />
                            </div>
                            <div onClick={() => onChangePractice(1)}>
                                <Icon icon={'arrow_forward'} />
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

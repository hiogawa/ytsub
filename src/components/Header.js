import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from '../stateUtils.js';
import { navigate } from '../Router.js';
import Icon from './Icon.js';
import Input from './Input.js';
import { parseYoutubeUrl } from '../utils.js';

const SubtitleSelect = ({ subtitles, selected, onChange, defaultText }) => (
    <select
        value={Number.isInteger(selected) ? selected : 'default'}
        onChange={(e) => onChange(Number(e.target.value))}
    >
        <option value={'default'} disabled>{ defaultText }</option>
        {
            Number.isInteger(selected) &&
            subtitles.map((subtitle, index) =>
                <option key={index} value={index}>
                    { subtitle.name }
                </option>
            )
        }
    </select>
)

const loadVideo = (value) => {
    let videoId = parseYoutubeUrl(value);
    if (videoId) {
        navigate(`watch/${videoId}`);
    } else {
        window.alert('Error: Your input is invalid.');
    }
}

export default connect(
    S => S,
    D => bindActionCreators(actions, D)
)(({ video, player, setSubtitle, displayPlayer, setPlaybackRate }) => {
    return (
        <div id="header">
            <button className='home-nav' onClick={() => navigate('')}>
                <Icon icon='home'/>
            </button>

            <button className='toggle-player' onClick={() => displayPlayer(!player.display)}>
                <span className='toggle-player__text'>{ player.display ? 'Hide' : 'Show' }</span>
                <Icon icon={ player.display ? 'videocam_off' : 'videocam' } />
            </button>

            <Input className='video-id-input'
                value={video.id}
                onValueEntered={loadVideo}
                placeholder='Video ID or URL'
            />

            <div className='language-select'>
                <SubtitleSelect
                    subtitles={video.subtitles}
                    selected={video.selected[0]}
                    onChange={(index) => setSubtitle(0, index)}
                    defaultText='-- 1st Language --'
                />
                <SubtitleSelect
                    subtitles={video.subtitles}
                    selected={video.selected[1]}
                    onChange={(index) => setSubtitle(1, index)}
                    defaultText='-- 2nd Language --'
                />
                <div>
                    <Icon icon='translate'/>
                </div>
            </div>

            <div className='playback-speed-select'>
                <select
                    value={player.state.playbackRate || 1}
                    onChange={(e) => setPlaybackRate(Number(e.target.value))}
                >
                    { (player.state.availablePlaybackRates || [1]).map(rate =>
                        <option key={rate} value={rate}>{rate}</option>)}
                </select>
                <div>
                    <Icon icon='av_timer'/>
                </div>
            </div>
        </div>
    );
});

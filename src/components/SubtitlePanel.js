import React, { Component } from 'react';
import _ from 'underscore';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions, getSelectedSubtitles } from '../stateUtils.js';
import { createEntries } from '../utils.js';
import TimedTextEntry from './TimedTextEntry.js';
import { PLAYING } from './YTPlayer.js';

class EntryList extends Component {
    constructor(props) {
        super(props);
        let { sub1, sub2 } = this.props;
        this.entries = createEntries(sub1.ttml, sub2.ttml);
        this.state = {
            loopIndex: null,
            practiceIndex: null,
        };
    }
    componentDidUpdate() {
        this.loopEntryIfNeeded();
    }
    loopEntryIfNeeded() {
        let { loopIndex } = this.state;
        if (loopIndex) {
            let { seekTo, currentTime } = this.props;
            let entry = this.entries[loopIndex];
            if (currentTime < entry.begin_num || entry.end_num < currentTime) {
                seekTo(entry.begin_num);
            }
        }
    }
    render() {
        let { seekTo, pause, currentTime, playerState } = this.props;
        let { practiceIndex, loopIndex } = this.state;
        let playerPlaying = playerState === PLAYING;

        // Two heuristics to find playing entry (1st one works aweful for auto caption timestamps)
        // let focusIndex = _.findIndex(this.entries, e => player.currentTime < e.end_num);
        let focusIndex = _.findLastIndex(this.entries, e => e.begin_num < currentTime);

        return (
            <div>
                { this.entries.map((entry, index) => {
                    let focusing = loopIndex === null && index === focusIndex;
                    let looping = index === loopIndex;
                    let practicing = index === practiceIndex;
                    let playing = (focusing || looping) && playerPlaying;
                    return (
                        <TimedTextEntry key={index}
                            onTogglePlay={() => {
                                if (playing) {
                                    pause();
                                // NOTE: unpause seems not useful, so comment out for now
                                // } else if (entry.begin_num <= currentTime && currentTime <= entry.end_num) {
                                //     unpause();
                                } else {
                                    if (!looping) { this.setState({ loopIndex: null }); }
                                    seekTo(entry.begin_num);
                                }
                            }}
                            onToggleLoop={() =>
                                this.setState({ loopIndex: looping ? null : index})}
                            onTogglePractice={() =>
                                this.setState({ practiceIndex: practicing ? null : index})}
                            onChangePractice={(increment) =>
                                this.setState({ practiceIndex: index + increment})}
                            {...{ focusing, looping, practicing, playing }}
                            {...entry}
                        />
                    );
                })}
            </div>
        )
    }
}

export default connect(
    S => {
        let [sub1, sub2] = getSelectedSubtitles(S);
        let condition;
        if (sub1 && sub2) {
            if ([sub1, sub2].some(_.matcher({ status: 'LOADING' }))) {
                condition = 'LOADING'
            } else {
                condition = 'SUCCESS'
            }
        } else {
            if (S.video.status.match(/LOADING|INITIAL/)) {
                condition = 'NOOP'
            } else {
                condition = 'NOT_SELECTED'
            }
        }
        let { state: { playerState } , currentTime } = S.player;
        return { condition, sub1, sub2, playerState, currentTime };
    },
    D => bindActionCreators(actions, D)
)(({ condition, sub1, sub2, seekTo, pause, unpause, playerState, currentTime }) => {
    /* eslint-disable react/display-name */
    let switcher = {
        NOOP:         () => null,
        LOADING:      () => <div className='subtitle-panel__spinner' />,
        NOT_SELECTED: () => 'Languages are not selected.',
        SUCCESS:      () =>
            // cf. https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html#recommendation-fully-uncontrolled-component-with-a-key
            <EntryList key={sub1.ttml + sub2.ttml} {...{ sub1, sub2, seekTo, pause, unpause, playerState, currentTime }} />
    };
    return <div className='subtitle-panel'>{ switcher[condition]() }</div>
    /* eslint-enable react/display-name */
});

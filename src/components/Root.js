import React from 'react';
import { Provider, connect } from 'react-redux';
import Header from './Header.js';
import PlayerPanel from './PlayerPanel.js';
import SubtitlePanel from './SubtitlePanel.js';
import DevTools from './DevTools.js';
import HomeModal from './HomeModal.js';

const AppSpinner = connect(
    ({  video: { status },
        home: { open },
        _persist: { rehydrated } }) => ({ status, open, rehydrated })
)(({ status, open, rehydrated }) => {
    if ((!rehydrated || (!open && status.match(/LOADING/)))) {
        return <div className='app-spinner'></div>;
    } else {
        return null;
    }
});

const Root = ({ store }) => (
    <Provider store={store}>
        <>
            <HomeModal />
            <AppSpinner />
            <Header />
            <div id="main">
                <PlayerPanel />
                <SubtitlePanel />
            </div>
            { PRODUCTION ? null : <DevTools /> }
        </>
    </Provider>
);

import { hot } from 'react-hot-loader/root';
export default hot(Root);

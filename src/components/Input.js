import React, { Component, createRef } from 'react';
import CN from 'classnames';
import Icon from './Icon.js';

class Input extends Component {
    constructor(props) {
        super(props);
        this.input = createRef();
    }
    render() {
        let { className, value, onValueEntered, icon, ...rest } = this.props;

        // aka. fully uncontrolled with a key
        return (
            <div className={CN('input-component', className)}>
                <input key={value}
                    defaultValue={value}
                    ref={this.input}
                    onKeyPress={(e) => e.key == 'Enter' && onValueEntered(this.input.current.value)}
                    {...rest} />
                <button onClick={() => onValueEntered(this.input.current.value)}>
                    <Icon {...{ icon }}/>
                </button>
            </div>
        )
    }
}

Input.defaultProps = {
    icon: 'search',
    type: 'text',
    autoCapitalize: 'none',
    autoComplete: 'off',
    autoCorrect: 'off',
    spellCheck: 'false'
};

export default Input;

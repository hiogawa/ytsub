import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CN from 'classnames';
import { actions } from '../stateUtils.js';
import YTPlayer from './YTPlayer.js';

// Integrate YTPlayer for our purpose
export default connect(
    S => ({ videoId: S.video.id, player: S.player }),
    D => bindActionCreators(actions, D)
)(({ videoId, player, $U }) =>
    // NOTE: we cannot really unmount YTPlayer (don't really need to as well), so just hide it.
    <div id="player-container" className={CN({ hidden: !player.display })}>
        <div id="player-aspect-ratio-fixer1">
            <div id="player-aspect-ratio-fixer2">
                <YTPlayer
                    id='player'
                    videoId={videoId}
                    request={player.request}
                    onReady={(state) => $U({ player: { state: { $set: state } }})}
                    onStateChange={(state) => $U({ player: { state: { $set: state } }})}
                    onChangeTime={(time) => $U({ player: { currentTime: { $set: time } } })}
                    onRequestDone={() => $U({ player: { request: { status: { $set: 'SUCCESS' } } } })}
                    options={{
                        height: '480', width: '853',
                        playerVars: {}
                    }}
                />
            </div>
        </div>
    </div>
);

import React, { Component, createRef } from 'react';
import _ from 'underscore';
import PT from 'prop-types';

const getYoutubeIframeAPI = _.memoize(() => new Promise(resolve => {
    window.onYouTubeIframeAPIReady = () => resolve(window.YT);

    let script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.src = 'https://www.youtube.com/iframe_api';
    let firstScript = document.getElementsByTagName('script')[0];
    firstScript.parentNode.insertBefore(script, firstScript);
}));

// cf: https://developers.google.com/youtube/iframe_api_reference#Events
export const UNSTARTED = -1;// eslint-disable-line no-unused-vars
export const ENDED = 0;     // eslint-disable-line no-unused-vars
export const PLAYING = 1;
export const PAUSED = 2;    // eslint-disable-line no-unused-vars
export const BUFFERING = 3; // eslint-disable-line no-unused-vars
export const CUED = 5;      // eslint-disable-line no-unused-vars

// Youtube Iframe player wrapper
export default class YTPlayer extends Component {
    constructor(props) {
        super(props);
        this.ref = createRef();
        this.player = null;
        this.interval = null;
        this.YT = null;
        this.ready = null;
    }
    componentDidMount() {
        this.ready = new Promise(resolve => {
            getYoutubeIframeAPI().then((YT) => {
                this.YT = YT;
                let options = _.extend({}, this.props.options, {
                    events: {
                        onReady: resolve,
                        onStateChange: () => this.props.onStateChange(this.getState()),
                        onPlaybackRateChange: () => this.props.onStateChange(this.getState()),
                    }
                });
                this.player = new this.YT.Player(this.ref.current, options);
            });
        });
        this.ready.then(() => {
            this.props.onStateChange(this.getState());
            this.interval = window.setInterval(() =>
                this.processInterval(), this.props.intervalDuration);
        })
    }
    componentWillUnmount() {
        window.clearInterval(this.interval);
    }
    componentDidUpdate(prevProps) {
        this.handleNewProps(prevProps, this.props);
    }
    handleNewProps(prevProps, nextProps) {
        this.ready.then(() => {
            let { method, args, status } = nextProps.request;
            if (status === 'PENDING') {
                if (method === 'seekTo') {
                    this.seekTo(...args);
                } else {
                    this.player[method](...args);
                }
                this.props.onRequestDone();
            }
            if (prevProps.videoId !== nextProps.videoId && nextProps.videoId) {
                this.player.cueVideoById(nextProps.videoId);
            }
        })
    }
    getState() {
        let keys = [ 'playerState', 'playbackRate', 'availablePlaybackRates' ];
        let results = keys.map(key => this.player[`get${key[0].toUpperCase()}${key.substr(1)}`]());
        return _.object(_.zip(keys, results));
    }
    processInterval() {
        if (this.player.getPlayerState() == PLAYING) {
            this.props.onChangeTime(this.player.getCurrentTime());
        }
    }
    stateChangedTo(status) {
        return new Promise(resolve => {
            let callback = (e) => {
                if (e.data === status) {
                    resolve();
                    this.player.removeEventListener('onStateChange', callback);
                }
            };
            this.player.addEventListener('onStateChange', callback);
        })
    }
    seekTo(time) {
        switch (this.player.getPlayerState()) {
            case PLAYING: {
                this.player.seekTo(time);
                break;
            }
            default: {
                this.stateChangedTo(PLAYING).then(() => this.player.seekTo(time));
                this.player.playVideo();
            }
        }
        this.props.onChangeTime(time);
    }
    render() {
        let { id, className } = this.props;
        return <div ref={this.ref} {...{ id, className }} />
    }
}

YTPlayer.propTypes = {
    videoId: PT.string, // Updatable
    request: PT.shape({ // Updatable
        method: PT.string,
        args: PT.array,
        status: PT.string
    }),
    options: PT.object,
    onReady: PT.func,
    onStateChange: PT.func,
    onChangeTime: PT.func,
    onRequestDone: PT.func,
    intervalDuration: PT.number,
    id: PT.string,
    className: PT.string,
};

YTPlayer.defaultProps = {
    intervalDuration: 500
};

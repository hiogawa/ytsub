module.hot && module.hot.accept();
import _ from 'underscore';

export const timeToNumber = (text) => {
    let f = (h_m_s_f) => ((h_m_s_f[0] * 60 + h_m_s_f[1]) * 60) + h_m_s_f[2] + h_m_s_f[3] * 0.001;
    return f(text.split(/[:.]/).map(Number));
}

export const shallowCompare = (x, y) => {
    if ([x, y].every(_.negate(_.isArray))) {
        x = _.pairs(x);
        y = _.pairs(y);
    }
    return (
        x.length == y.length &&
        _.range(x.length).every(i => x[i] == y[i])
    );
}

export const parseYoutubeUrl = (value) => {
    let videoId;
    if (value.length == 11) {
        videoId = value;
    } else if (value.match(/youtube\.com|youtu\.be/)) {
        let url = new URL(value);
        if (url.hostname == 'youtu.be') {
            videoId = url.pathname.substr(1);
        } else {
            videoId = url.search.match(/v=(.{11})/)[1];
        }
    }
    return videoId;
}

// TODO: consider cacheing via serviceWorker
export const memoizedFetchText = _.memoize(async (...args) => fetch(...args).then(resp => {
    if (!resp.ok) { throw null; }
    return resp.text();
}));

export const getConfig = _.memoize(async (videoId) => {
    let text = await memoizedFetchText(`${PROXY_PREFIX}https://www.youtube.com/watch?v=${videoId}`);
    // cf. https://github.com/rg3/youtube-dl/blob/master/youtube_dl/extractor/youtube.py#L1276
    let mobj = text.match(/;ytplayer\.config\s*=\s*({.+?});ytplayer/) // www.youtube.com
            || text.match(/ytInitialPlayerConfig\s*=\s*({.+?});/);    // m.youtube.com
    return JSON.parse(mobj[1]);
});

export const extractVideoInfoFromConfig = (config) => {
    const player_response = JSON.parse(config.args.player_response);
    return _.pick(player_response.videoDetails, ['author', 'title', 'channelId']);
}

export const getSubtitleList = (config) => {
    let player_response = JSON.parse(config.args.player_response);
    let tracks = player_response.captions.playerCaptionsTracklistRenderer.captionTracks;
    let translations = player_response.captions.playerCaptionsTracklistRenderer.translationLanguages;
    if (!tracks) { return []; }
    let list = [];
    let getText = (obj) => {
        let inner = obj.name || obj.languageName; // "track" or "translation"
        return inner.simpleText || inner.runs[0].text; // www.youtube.com or m.youtube.com
    }
    tracks.forEach(track => {
        // in some cases, domain is missing originally
        if (!track.baseUrl.startsWith('https')) {
            track.baseUrl = 'https://www.youtube.com' + track.baseUrl;
        }
        list.push({
            url: `${track.baseUrl}&fmt=ttml`,
            name: getText(track),
            type: 'track',
            code: track.languageCode,
            auto: track.kind == 'asr', // i.e. auto generated
            ttml: null,
            status: 'INITIAL',
        });
    });
    tracks.forEach(track => {
        if (track.isTranslatable) {
            translations.forEach(t => {
                list.push({
                    url: `${track.baseUrl}&fmt=ttml&tlang=${t.languageCode}`,
                    name: `${getText(t)} (Translations from ${getText(track)})`,
                    type: 'translation',
                    code: t.languageCode,
                    ttml: null,
                    status: 'INITIAL',
                })
            })
        }
    });
    return list;
};

// Hard-code some heristics for my preference
export const findDefaultSubtitles = (list, [lang1, lang2]) => {
    let preferences = [
        [{ type: 'track', code: lang1, auto: false }, { type: 'track', code: lang2, auto: false }],
        [{ type: 'track', code: lang1, auto: true }, { code: lang2 }],
        [{ type: 'track' }, { type: 'translation', code: lang2 }],
        [{ type: 'track' }, { type: 'translation', code: lang1 }]
    ];
    for (let i = 0; i < preferences.length; i++) {
        let pair = preferences[i];
        let obj1 = _.findWhere(list, pair[0]);
        let obj2 = _.findWhere(list, pair[1]);
        if (obj1 && obj2) {
            return [obj1, obj2].map(x => _.findIndex(list, y => x == y));
        }
    }
};

export const createEntries = (ttml1, ttml2) => {
    let [ lang1_ps, lang2_ps ] = [ttml1, ttml2]
            .map(str => {
                let doc = (new DOMParser()).parseFromString(str, 'text/xml');
                // NOTE: Handle subtitle containing "<p ...> ... <br /> ... </p>"
                Array.from(doc.getElementsByTagName('br')).forEach(br => br.replaceWith(' '));
                return doc.getElementsByTagName('p');
            });
    let pair_ps = [];
    let entries;
    // Select matching entries (by begin time) from already sorted arrays in O(n)
    for (let i = 0, j = 0; i < lang1_ps.length; i++) {
        let lang1_p = lang1_ps[i];
        let lang2_p = null;
        for (; j < lang2_ps.length; j++) {
            let x = timeToNumber(lang1_p.attributes['begin'].value);
            let y = timeToNumber(lang2_ps[j].attributes['begin'].value);
            if (x < y) { break; }
            else if (x == y) { lang2_p = lang2_ps[j]; }
        }
        pair_ps.push({ lang1_p, lang2_p });
    }
    entries = pair_ps.map(({ lang1_p, lang2_p }) => {
        let begin = lang1_p.attributes['begin'].value,
            end = lang1_p.attributes['end'].value,
            begin_num = timeToNumber(begin),
            end_num = timeToNumber(end);
        return {
            begin, begin_num, end, end_num,
            lang1_text: lang1_p.textContent,
            lang2_text: lang2_p ? lang2_p.textContent : '',
        };
    });
    return entries;
};

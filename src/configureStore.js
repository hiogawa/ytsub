import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import update from 'immutability-helper';
import { bindActionCreators } from 'redux';
import DevTools from './components/DevTools.js';
import { actions, initialState } from './stateUtils.js';

// Simple reducer exploiting "update" api
const reducer = (state, action) => (action.type == '$U' ? update(state, action.command) : state);
const dispatchUpdate = (dispatch) => (command) => dispatch({ type: '$U', command });

// cf. https://github.com/reduxjs/redux-thunk/blob/master/src/index.js
const myThunk = ({ dispatch, getState }) => next => action => {
    if (typeof action === 'function') {
        return action(dispatch, getState, dispatchUpdate(dispatch));
    }
    return next(action);
};

const enhancer = PRODUCTION
    ? applyMiddleware(myThunk)
    : compose(applyMiddleware(myThunk), DevTools.instrument());

const persistConfig = {
    key: 'root',
    storage,
    whitelist: [ 'history' ]
};

const persistedReducer = persistReducer(persistConfig, reducer)

export default () => {
    let store = createStore(persistedReducer, initialState, enhancer);
    let persistor = persistStore(store);
    if (!PRODUCTION) {
        window.DEV = {
            store,
            persistor,
            actions: bindActionCreators(actions, store.dispatch)
        };
    }
    return { store, persistor };
}

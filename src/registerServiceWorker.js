import 'file-loader?name=serviceWorker.js!./serviceWorker.js';

export default () => {
    if ('serviceWorker' in window.navigator) {
        window.addEventListener('load', () => {
            window.navigator.serviceWorker.register(`${PUBLIC_PATH}serviceWorker.js`).then(
                registration => console.log('SW registered: ', registration), /* eslint-disable-line no-console */
                err => console.log('SW registration failed: ', err)           /* eslint-disable-line no-console */
            );
        });
    }
}

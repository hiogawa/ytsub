module.hot && module.hot.decline();
import Backbone from 'backbone';
import _ from 'underscore';
import { bindActionCreators } from 'redux';
import { actions } from './stateUtils.js';

// Router (i.e. url state) is outside of redux's control
export const navigate = (path, options = {}) => {
    Backbone.history.navigate(path, _.extend({ trigger: true }, options));
}
export const navigateBack = () => window.history.back();

const Router = Backbone.Router.extend({
    initialize(store) {
        window.history.replaceState({ isInitialLoad: true }, window.document.title, window.location.href);
        this.actions = bindActionCreators(actions, store.dispatch);
    },
    start() {
        Backbone.history.start({ root: PUBLIC_PATH });
    },

    routes: {
        '': 'home',
        'watch/*video_id': 'watch',
        '*path': 'notFound',
    },
    home() {
        let isInitialLoad = window.history.state && window.history.state.isInitialLoad;
        this.actions.openHome(true, isInitialLoad);
    },
    watch(videoId) {
        this.actions.openHome(false, false);
        this.actions.setVideoIfNeeded(videoId);
    },
    notFound() {
        navigate('', { replace: true, state: { isInitialLoad: true } });
    },
});

export default Router;

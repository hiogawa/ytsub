import './scss/index.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/Root.js';
import configureStore from './configureStore.js';
import registerServiceWorker from './registerServiceWorker.js';
import { initializeWatchers } from './watcher.js';
import Router from './Router.js';

registerServiceWorker();
const { store } = configureStore();
let router = new Router(store);
initializeWatchers(store, router);
ReactDOM.render(<Root store={store} />, document.getElementById('app'));

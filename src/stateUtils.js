module.hot && module.hot.accept();
import _ from 'underscore';
import { shallowCompare, memoizedFetchText, getConfig, getSubtitleList, findDefaultSubtitles } from './utils.js';

export const initialState = {
    video: {
        id: null, // '34STG-b8rnc'
        config: null, // { ... }
        status: 'INITIAL', // 'INITIAL', 'ERROR', 'LOADING', 'SUCCESS' ...
        subtitles: [
        //    {
        //       status: 'INITIAL', ttml: '', url: '', ...
        //    },
        //    ...
        ],
        selected: [null, null], // [n, m] refers two selected subtitles by their indeces in the "subtitles" array
    },
    player: {
        display: true,
        currentTime: null,
        state: {},
        request: {} // { seek: 12.3, status: 'PENDING' or 'SUCCESS } or { pause: true, ...}
    },
    home: {
        open: false,
        isInitialLoad: false,
        preferredLanguages: ['ru', 'en'],
    },
    history: [
        // { id, config } // NOTE: the subtitle url from config gets stale so use it only for showing video info
    ]
};

// State selector
export const getSelectedSubtitles = (S) => {
    let { subtitles, selected } = S.video;
    return selected.map(_.propertyOf(subtitles));
}

// Actions (or Actions creators)
export const actions = {
    $U: (command) => (D, S, U) => U(command), // Such a shortcut

    setVideoIfNeeded: (videoId) => async (D, S) => {
        let { id, status } = S().video;
        if (id !== videoId || status !== 'SUCCESS') {
            D(actions.setVideo(videoId));
        }
    },
    setVideo: (videoId) => async (D, S, U) => {
        let reinitVideo = _.extend({}, initialState.video, {
            id: videoId,
            status: 'LOADING'
        });
        let reinitPlayer = _.pick(initialState.player, ['currentTime', 'request']);
        U({
            video: { $merge: reinitVideo },
            player: { $merge: reinitPlayer }
        });

        try {
            let config = await getConfig(videoId);
            await D(actions.updateHistory(videoId, config));
            let subtitles = getSubtitleList(config);
            U({
                video: { $merge: {
                    config,
                    subtitles,
                    status: 'SUCCESS'
                }},
            });

            let { preferredLanguages } = S().home;
            let result = findDefaultSubtitles(subtitles, preferredLanguages);
            if (result) {
                await Promise.all([
                    D(actions.setSubtitle(0, result[0])),
                    D(actions.setSubtitle(1, result[1]))
                ]);
            }
        } catch (e) {
            console.error(e); // eslint-disable-line no-console
            U({
                video: { $merge: {
                    status: 'ERROR'
                }}
            });
        }
    },
    setSubtitle: (key, value) => async (D, S, U) => {
        U({
            video: {
                selected: { [key]: { $set: value } }
            }
        });

        let subtitle = S().video.subtitles[value];
        if (subtitle.status == 'INITIAL') {
            U({
                video: {
                    subtitles: { [value]: { status: { $set: 'LOADING' } } }
                }
            });

            let ttml = await memoizedFetchText(subtitle.url);
            U({
                video: { subtitles: { [value]: {
                    $merge: {
                        ttml,
                        status: 'SUCCESS'
                    }
                }}}
            });
        }
    },

    displayPlayer: (b) => async (D, S, U) => {
        U({ player: { display: { $set: b } } });
    },
    requestToPlayer: (method, ...args) => async (D, S, U) => {
        let newRequest = { method, args, status: 'PENDING' };
        if (!shallowCompare(newRequest, S().player.request)) {
            U({ player: { request: { $set: newRequest } } });
        }
    },
    seekTo: (time) => async (D) => D(actions.requestToPlayer('seekTo', time)),
    pause: () => async (D) => D(actions.requestToPlayer('pauseVideo')),
    unpause: () => async (D) => D(actions.requestToPlayer('playVideo')),
    setPlaybackRate: (rate) => async (D) => D(actions.requestToPlayer('setPlaybackRate', rate)),

    openHome: (open, isInitialLoad) => async (D, S, U) => {
        U({ home: { $merge: { open, isInitialLoad } } });
    },

    updateHistory: (id, config) => async (D, S, U) => {
        let { history } = S();
        if (!_.find(history, _.matcher({ id }))) {
            U({ history: { $unshift: [{ id, config }]} });
        }
    },
    deleteHistory: (id) => async (D, S, U) => {
        let { history } = S()
        if (_.find(history, _.matcher({ id }))) {
            let nextHistory = _.reject(history, _.matcher({ id }));
            U({ history: { $set: nextHistory } });
        }
    },
};
